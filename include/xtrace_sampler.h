#ifndef _XTRACE_SAMPLER_
#define _XTRACE_SAMPLER_

namespace XTraceSampler {

	namespace SamplingSetting {
		extern bool sample;
		extern double samplingRate;

		void getDefaultSetting();
		bool getSample();
		double getSamplingRate();
	}


	class Sampler {
		public:
		Sampler();
		virtual bool sample();
		virtual void setParameter(double);
	};


	class UniformSampler: public Sampler {	
		private:
		double p;
			
		public:
		UniformSampler();
		UniformSampler(double p, unsigned int seed);
		bool sample();
		void setParameter(double p);
	};


	extern int startedTracesNum;
	extern int samplingDecisionsNum;
	extern Sampler* sampler;

	void createSampler();
	void initSampler();
	void incrStartedTraces();
	void incrSamplingDecisions();
	bool sample();
	int getStartedTracesNumber();
	int getSamplingDecisionsNumber();
}

#endif
