#include <cstdlib>
#include <ctime>
#include <libconfig.h++>
#include <iostream>
#include "xtrace_sampler.h"

bool XTraceSampler::SamplingSetting::sample = true;
double XTraceSampler::SamplingSetting::samplingRate = 1.0;

void XTraceSampler::SamplingSetting::getDefaultSetting() {
    libconfig::Config config;
	try {
		config.readFile("../resources/reference.cfg");
		const libconfig::Setting &root = config.getRoot();
		const libconfig::Setting &sampling = root["xtrace"]["sampling"];
		sample = sampling.lookup("sample");
		samplingRate = sampling.lookup("sampling_rate");
	}
	catch(const std::exception& e) {
		std::cerr << e.what() << '\n';
	}
}

bool XTraceSampler::SamplingSetting::getSample() {
	return sample;
}

double XTraceSampler::SamplingSetting::getSamplingRate() {
	return samplingRate;	
}

XTraceSampler::Sampler::Sampler() {}

bool XTraceSampler::Sampler::sample() {
	return true;
}

void XTraceSampler::Sampler::setParameter(double p) {}

XTraceSampler::UniformSampler::UniformSampler() {
	this -> p = SamplingSetting::getSamplingRate();
	srand(static_cast<unsigned int>(clock()));
}

XTraceSampler::UniformSampler::UniformSampler(double p, unsigned int seed) {
	this -> p = p;
	srand(seed);
}

bool XTraceSampler::UniformSampler::sample() {
    if ((double)rand() / RAND_MAX < this -> p) {
        return true;
    }
    else {
    	return false;
	}
}

void XTraceSampler::UniformSampler::setParameter(double p) {
	this -> p = p;
}

int XTraceSampler::startedTracesNum = 0;
int XTraceSampler::samplingDecisionsNum = 0;
XTraceSampler::Sampler* XTraceSampler::sampler = NULL;

void XTraceSampler::createSampler() {
    initSampler();
	SamplingSetting::getDefaultSetting();
	if (SamplingSetting::getSample()) {
		sampler = new UniformSampler();
	}
	else {
    	sampler = new Sampler();
	}
}

void XTraceSampler::initSampler() {
	sampler = NULL;
	startedTracesNum = 0;
    samplingDecisionsNum = 0;
}

void XTraceSampler::incrStartedTraces() {
	startedTracesNum++;
}

void XTraceSampler::incrSamplingDecisions() {
	samplingDecisionsNum++;
}

bool XTraceSampler::sample() {
	if (sampler == NULL) {
    	createSampler();
    }

	incrSamplingDecisions();
	if (sampler -> sample()) {
    	incrStartedTraces();
    	return true;
	}
    else {
    	return false;
	}
}

int XTraceSampler::getStartedTracesNumber() {
	return startedTracesNum;
}

int XTraceSampler::getSamplingDecisionsNumber() {
	return samplingDecisionsNum;
}