#include <catch2/catch.hpp>

#include "xtrace_sampler.h"
#include "xtrace.h"
#include "pubsub.h"
#include "xtrace_baggage.h"

TEST_CASE( "Directly test sample = false", "[no-sample]" ) {
    XTraceSampler::initSampler();

    int sampleCallsNumber = 1000;
    XTraceSampler::sampler = new XTraceSampler::Sampler();

    for (int i = 0; i < sampleCallsNumber; i++) {
        REQUIRE(XTraceSampler::sample());
    }

    REQUIRE(XTraceSampler::getStartedTracesNumber() == sampleCallsNumber);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == sampleCallsNumber);
}

TEST_CASE( "Directly test sample = true, p = 0.5", "[sample]" ) {
    XTraceSampler::initSampler();

    int tracesNumber = 1000;
    double samplingRate = 0.48;
    XTraceSampler::sampler = new XTraceSampler::UniformSampler(0.5, 20);

    for (int i = 0; i < tracesNumber; i++) {
        XTraceSampler::sample();
    }

    int decisionsNum = XTraceSampler::getSamplingDecisionsNumber();
    REQUIRE((double)XTraceSampler::getStartedTracesNumber() / decisionsNum == samplingRate);
    REQUIRE(decisionsNum == tracesNumber);
}

TEST_CASE( "Check results for sample = true", "[sample]" ) {
    XTraceSampler::initSampler();

    int tracesNumber = 10;
    XTraceSampler::sampler = new XTraceSampler::UniformSampler(0.2, 20);
    bool expectedDecisions[10] = {true, false, false, false, false, false, false, false, false, false};

    for (int i = 0; i < tracesNumber; i++) {
        REQUIRE(XTraceSampler::sample() == expectedDecisions[i]);
    }
}

