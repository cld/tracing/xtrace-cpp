#include <catch2/catch.hpp>

#include "xtrace_sampler.h"
#include "xtrace.h"
#include "pubsub.h"
#include "xtrace_baggage.h"

TEST_CASE( "Test sample = false", "[no-sample]" ) {
    XTraceSampler::initSampler();

    int tracesNumber = 1000;

    for (int i = 0; i < tracesNumber; i++) {
        uint64_t oldTaskID = XTraceBaggage::GetTaskID();
        XTrace::StartTrace("Testing trace");
        REQUIRE(XTraceBaggage::HasTaskID());
        REQUIRE(XTraceBaggage::GetTaskID() != oldTaskID);
        XTrace::log("Ending trace");
    }

    REQUIRE(XTraceSampler::getStartedTracesNumber() == tracesNumber);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == tracesNumber);
}

TEST_CASE( "Test sample = true, p = 0", "[sample]" ) {
    XTraceSampler::initSampler();

    int tracesNumber = 1000;
    XTraceSampler::sampler = new XTraceSampler::UniformSampler(0, static_cast<unsigned int>(clock()));

    for (int i = 0; i < tracesNumber; i++) {
        XTrace::StartTrace("Testing trace");
        XTrace::log("Ending trace");
        REQUIRE(!XTraceBaggage::HasTaskID());
        REQUIRE(!XTraceBaggage::HasParentEventIDs());
    }

    REQUIRE(XTraceSampler::getStartedTracesNumber() == 0);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == tracesNumber);
}

TEST_CASE( "Test sample = true, p = 1", "[sample]" ) {
    XTraceSampler::initSampler();

    int tracesNumber = 1000;
    XTraceSampler::sampler = new XTraceSampler::UniformSampler(1, static_cast<unsigned int>(clock()));

    for (int i = 0; i < tracesNumber; i++) {
        XTrace::StartTrace("Testing trace");
        XTrace::log("Ending trace");
        REQUIRE(XTraceBaggage::HasTaskID());
        REQUIRE(XTraceBaggage::HasParentEventIDs());
    }

    REQUIRE(XTraceSampler::getStartedTracesNumber() == tracesNumber);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == tracesNumber);
}

TEST_CASE( "Check has parents for sample = true", "[sample]" ) {
    XTraceSampler::initSampler();

    int tracesNumber = 2;
    bool expectedDecisions[2] = {true, false};
    XTraceSampler::sampler = new XTraceSampler::UniformSampler(1, static_cast<unsigned int>(clock()));

    for (int i = 0; i < tracesNumber; i++) {
        XTraceSampler::sampler -> setParameter(1 - i);
        XTrace::StartTrace("Testing trace");
        XTrace::log("Ending trace");

        REQUIRE(XTraceBaggage::HasTaskID() == expectedDecisions[i]);
        REQUIRE(XTraceBaggage::HasParentEventIDs() == expectedDecisions[i]);
    }
}

TEST_CASE( "Check no parents for sample = true", "[sample]" ) {
    XTraceSampler::initSampler();

    int tracesNumber = 2;
    bool expectedDecisions[2] = {false, true};
    XTraceSampler::sampler = new XTraceSampler::UniformSampler(1, static_cast<unsigned int>(clock()));

    for (int i = 0; i < tracesNumber; i++) {
        XTraceSampler::sampler -> setParameter(i);
        XTrace::StartTrace("Testing trace");
        XTrace::log("Ending trace");

        REQUIRE(XTraceBaggage::HasTaskID() == expectedDecisions[i]);
        REQUIRE(XTraceBaggage::HasParentEventIDs() == expectedDecisions[i]);
    }

    PubSub::shutdown();
	PubSub::join();
}
